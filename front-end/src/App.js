import './App.css';
import ProjectPage from './pages/project-page/project_page_dev'
import MainPage from './pages/main-page/main-page-dev'
import {
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div className="App">
			<Switch>
				<Route exact path='/' component={MainPage} />
				<Route exact path='/project' component={ProjectPage} />	
			</Switch>
    </div>
  );
}

export default App;
