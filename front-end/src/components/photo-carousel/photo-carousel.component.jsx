import React, { useState } from 'react';
import './photo-carousel.styles.scss'
import { ReactComponent as ArrowLeft } from '../../../node_modules/bootstrap-icons/icons/arrow-left-circle.svg'
import { ReactComponent as ArrowRight } from '../../../node_modules/bootstrap-icons/icons/arrow-right-circle.svg'

const PhotoCarousel = ({images}) => {
	const [current, setCurrent] = useState(0);

	if (!images || !(images instanceof Array) || images.length === 0) return null;
	const length = images.length;


	const nextSlide = () => {
		setCurrent(current === length - 1 ? 0 : current + 1);
	}

	const prevSlide = () => {
		setCurrent(current === 0 ? length - 1 : current - 1);
	}

	return (
		<section className='slider'>
			<ArrowLeft className="left-arrow" onClick={prevSlide} />
			{
				images.map((image, index) => {
					return (
						<div 
							className={index === current ? 'slide active' : 'slide'} 
							key={index}
						>
							{index === current && (<img className="image" alt="cool" src={image} ></img>)}
						</div>
					)
				})
			}
			<ArrowRight className="right-arrow" onClick={nextSlide} />
		</section>
	)
}

export default PhotoCarousel