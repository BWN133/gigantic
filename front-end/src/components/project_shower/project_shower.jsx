import React from 'react'
import '../../assets/css/blog.css'
import '../../assets/css/dist/css/bootstrap.min.css'
import './project_shower.scss'


const ProjectShower = ( { description, url, title} ) => {
    return (
        <div style = {{ 
          backgroundImage: `url("${url}ur")` 
        }} class="p-4 p-md-5 mb-4 text-white rounded bg-dark shower">
          {/* /*<img alt='test' src="http://bbs.jf311.com/data/attachment/forum/201505/25/113407e6v4pom4yen44ee4.jpg" />*/}
        <div class="col-md-6 px-0">
          <h2 class="display-4 fst-italic">{title}</h2>
					{
						{description} 
							? <p class="lead my-3">{description}</p>
							: null
					}
        </div>
      </div>
    )
}

export default ProjectShower;