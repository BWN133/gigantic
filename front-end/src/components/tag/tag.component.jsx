import React from 'react'
import './tag.styles.scss'

const Tag = ({children, ...otherProps}) => {
	return (
		<div className='tag'>
			{children}
		</div>
	)
}

export default Tag;