import React, {Component} from 'react';
import '../../assets/css/headers.css'
import ProjectShower from '../../components/project_shower/project_shower'
import InfiniteScroll from 'react-infinite-scroll-component';

class MainPage extends Component {
	constructor(props) {
		super(props);
    //TODO: Change fetch data
		this.state = {
			arr: [1,2,3,4,5,6,7,8,9],
			searchField: '',
			posts: [],
			offset: 0,
		}

	}

	componentDidMount() {
		fetch(`http://localhost:8000/posts?limit=20`)
		.then(data => data.json())
		.then(posts => {this.setState({
			...this.state,
			posts
		})})
	}

  fetchMoreData = () => {
		this.setState({
			...this.state,
			offset: this.state.offset + 20
		})
		fetch(`http://localhost:8000/posts?limit=20&offset=${this.state.offset}`)
			.then(data => data.json())
			.then(newPosts => {this.setState({
				...this.state,
				posts: [...this.state.posts, ...newPosts]
			})});
  }

	handleSearchInput = (event) => {
		this.setState({
			...this.state,
			searchField: event.target.value
		}, () => {
			console.log(this.state.searchField)
		})
	}
	
	render() {
		return (
			<div>
			<header className="p-3 bg-dark text-white">
    <div className="container">
      <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">

        <ul className="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <li><a  className="nav-link px-2 text-secondary">Home</a></li>
          <li><a  className="nav-link px-2 text-white">Features</a></li>
          <li><a  className="nav-link px-2 text-white">Pricing</a></li>
          <li><a  className="nav-link px-2 text-white">FAQs</a></li>
          <li><a  className="nav-link px-2 text-white">About</a></li>
        </ul>

        <form className="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
          <input 
						type="search" 
						className="form-control form-control-dark" 
						placeholder="Search..." 
						aria-label="Search" 
						onChange={this.handleSearchInput}
					/>
        </form>

        <div className="text-end">
          <button type="button" className="btn btn-outline-light me-2">Login</button>
          <button type="button" className="btn btn-warning">Sign-up</button>
        </div>
      </div>
    </div>
  </header>
	<ProjectShower />
	<InfiniteScroll
			dataLength={this.state.posts.length}
			loader={<h4>Loading...</h4>}
			hasMore={true}
      next = {this.fetchMoreData}
  		endMessage={
    <p style={{ textAlign: 'center' }}>
      <b>Yay! You have seen it all</b>
    </p>
  }
	>
      {/* TODO: Use fetch data */}
			{this.state.posts.map((post) => <ProjectShower description = {post.body}  url = "http://bbs.jf311.com/data/attachment/forum/201505/25/113407e6v4pom4yen44ee4.jpg" title = {post.title}/>)}
	</InfiniteScroll>
	</div>
  
		)
	}
	
}

// <a href="/" className="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
// <svg className="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"/></svg>

export default MainPage;