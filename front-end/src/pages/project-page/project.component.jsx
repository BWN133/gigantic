import React, { Component } from 'react';
import './project.styles.scss'
import PhotoCarousel from '../../components/photo-carousel/photo-carousel.component'
import Tag from '../../components/tag/tag.component'

class ProjectPage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			title: "",
			description: "",
			photo_links: [],
			tags: [],
		}
	}

	componentDidMount() {
		fetch('http://localhost:8000/projects')
			.then(res => res.json())
			.then(
				(result) => {
					console.log(result)
					this.setState({
						title: result.title,
						description: result.description,
						photo_links: result.photo_links,
						tags: result.tags,
					});
				},
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
				}
			)
	}

	render() {
		return (
			<div className="project-page">
				<h1 className='title'>{this.state.title}</h1>
				<PhotoCarousel images={this.state.photo_links} />
				<h2>Description</h2>
				<div className="description">
					{this.state.description}
				</div>
				<h2>Skills Required</h2>
				<div className="tag-container">
					{
						(!this.state.tags || !(this.state.tags instanceof Array) || this.state.tags.length === 0)
							? null
							: this.state.tags.map(tag => <Tag>{tag}</Tag>)
					}
				</div>
			</div>
		);
	}
}

export default ProjectPage;