const mongoose = require('mongoose');
const { Schema } = mongoose

const PostSchema = new Schema({
	id: {type: Number},
	userId: {type: Number},
	title: {type: String},
	body: {type: String},
})

PostSchema.index({title: 'text'});

module.exports = mongoose.model('Post', PostSchema, 'posts')