const express = require('express')
const router = express.Router()
const Post = require('../models/Post')

router.get('/', async (req, res, next) => {
	const limit = parseInt(req.query.limit)
	const offset = parseInt(req.query.offset)
	const { search } = req.query;
	let posts;
	if (!search) {
		posts = await Post.find({}, null, {limit, skip: offset});
	} else {
		posts =	await Post.find({$text: {$search: search}}, null, {limit, skip: offset}); 
	}
	res.status(200).send(posts)
})

module.exports = router