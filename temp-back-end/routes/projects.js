const express = require('express')
const router = express.Router();

router.get('/', (req, res, next) => {
	res.status(200).send({ 
		title: "test",
		description: "blablablabla",
		photo_links: [
			"https://static2.srcdn.com/wordpress/wp-content/uploads/2020/07/Breaking-Bad-Werner-Heisenberg.jpg?q=50&fit=crop&w=960&h=500&dpr=1.5",
			"https://images.unsplash.com/photo-1626274892452-91186ac3f0eb?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
			"https://images.unsplash.com/photo-1606787619248-f301830a5a57?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
			"https://images.unsplash.com/photo-1626171617945-2067f6c577b6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"

		],
		tags: [
			"C++", 'Java', "python", "spring", "nodejs","C++", 'Java', "python", "spring", "nodejs","C++", 'Java', "python", "spring", "nodejs",
			"C++", 'Java', "python", "spring", "nodejs","C++", 'Java', "python", "spring", "nodejs","C++", 'Java', "python", "spring", "nodejs",
			"C++", 'Java', "python", "spring", "nodejs","C++", 'Java', "python", "spring", "nodejs","C++", 'Java', "python", "spring", "nodejs",
			"C++", 'Java', "python", "spring", "nodejs","C++", 'Java', "python", "spring", "nodejs","C++", 'Java', "python", "spring", "nodejs",
			"C++", 'Java', "python", "spring", "nodejs","C++", 'Java', "python", "spring", "nodejs","C++", 'Java', "python", "spring", "nodejs"
		],
	})	
})

module.exports = router;